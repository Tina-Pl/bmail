from google.appengine.ext import ndb


class Message(ndb.Model):
    author = ndb.StringProperty()
    email = ndb.StringProperty()
    message = ndb.TextProperty()
    sent_message = ndb.TextProperty()
    sent_to = ndb.StringProperty()
    created = ndb.DateTimeProperty(auto_now_add=True)
    deleted = ndb.BooleanProperty(default=False)
