#!/usr/bin/env python
import os
import jinja2
import webapp2
import time
import json
from models import Message
from google.appengine.api import users
from google.appengine.api import urlfetch

template_dir = os.path.join(os.path.dirname(__file__), "templates")
jinja_env = jinja2.Environment(loader=jinja2.FileSystemLoader(template_dir), autoescape=False)


class BaseHandler(webapp2.RequestHandler):

    def write(self, *a, **kw):
        return self.response.out.write(*a, **kw)

    def render_str(self, template, **params):
        t = jinja_env.get_template(template)
        return t.render(params)

    def render(self, template, **kw):
        return self.write(self.render_str(template, **kw))

    def render_template(self, view_filename, params=None):
        if params is None:
            params = {}
        template = jinja_env.get_template(view_filename)
        return self.response.out.write(template.render(params))


class MainHandler(BaseHandler):
    def get(self):
        user = users.get_current_user()

        if user:
            logged_in = True
            logout_url = users.create_logout_url('/')

            params = {"logged_in": logged_in, "logout_url": logout_url, "user": user}
        else:
            logged_in = False
            login_url = users.create_login_url('/')

            params = {"logged_in": logged_in, "login_url": login_url, "user": user}

        return self.render_template("login.html", params)


class BmailHandler(BaseHandler):
    def get(self):
        return self.render_template("bmail.html")


class ComposeMessageHandler(BaseHandler):
    def get(self):
        return self.render_template("compose_message.html")

    def post(self):
        sent_to = self.request.get("sent_to")
        sent_message = self.request.get("sent_message")

        message = Message(sent_to=sent_to, sent_message=sent_message)
        message.put()

        return self.redirect_to("sentmail-site")


class SentMessagesHandler(BaseHandler):
    def get(self):
        sentmail_list = Message.query(Message.deleted == False).fetch()
        params = {"sentmail_list": sentmail_list}
        return self.render_template("sent_messages.html", params=params)

    def post(self):
        sent_to = self.request.get("sent_to")
        sent_message = self.request.get("sent_message")

        message = Message(sent_to=sent_to, sent_message=sent_message)
        message.put()

        return self.redirect_to("sentmail-site")


class WeatherHandler(BaseHandler):
    def get(self):

        q = self.request.get('q')
        if q is not None and q !='':
            city = q
        else:
            city = 'Ljubljana'

        c = self.request.get('c')
        if c is not None and c != '':
            country = c
        else:
            country = 'SI'

        url = "http://api.openweathermap.org/data/2.5/weather?q={city},{country}&units=metric&appid=17fe5e86133e421df334e2b44d0cdd91".format(
            city=city, country=country)
        result = urlfetch.fetch(url)

        data = json.loads(result.content)

        params = {"data": data,
                  "city": city,
                  "country": country}

        self.render_template("weather.html", params)


class MessageDeleteHandler(BaseHandler):
    def get(self, message_id):
        message = Message.get_by_id(int(message_id))
        params = {"message": message}
        return self.render_template("delete_message.html", params=params)

    def post(self, message_id):
        message = Message.get_by_id(int(message_id))
        message.deleted = True
        message.put()
        return self.redirect_to("sentmail-site")


app = webapp2.WSGIApplication([
    webapp2.Route('/', MainHandler),
    webapp2.Route('/bmail', BmailHandler, name="bmail-site"),
    webapp2.Route('/compose', ComposeMessageHandler, name="composemail-site"),
    webapp2.Route('/sent', SentMessagesHandler, name="sentmail-site"),
    webapp2.Route('/weather', WeatherHandler, name="weather-site"),
    webapp2.Route('/message/<message_id:\d+>/delete', MessageDeleteHandler, name="message-delete"),
], debug=True)